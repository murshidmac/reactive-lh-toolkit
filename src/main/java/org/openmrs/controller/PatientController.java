package org.openmrs.controller;

import lombok.RequiredArgsConstructor;
import org.hl7.fhir.dstu3.model.IdType;
import org.openmrs.model.CPatient;
import org.openmrs.repository.PatientRepository;
import org.openmrs.util.GeneralUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.cassandra.core.ReactiveCassandraOperations;
import org.springframework.data.cassandra.core.query.Criteria;
import org.springframework.data.cassandra.core.query.CriteriaDefinition;
import org.springframework.data.cassandra.core.query.Query;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("Patient")
@RequiredArgsConstructor
public class PatientController {

  private final PatientRepository patientRepository;
  private final ReactiveCassandraOperations reactiveCassandraOperations;

  @PutMapping(value = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
  public Mono<ResponseEntity<CPatient>> update(@PathVariable String id, @RequestBody Mono<CPatient> patientMono) {
    Mono<CPatient> updatedPatientMono = patientMono
            .doOnNext(patient -> patient.setPatientId(id));
    return patientRepository
            .findById(id)
            .flatMap(patient -> updatedPatientMono
                    .flatMap(updatedPatient -> {
                      BeanUtils.copyProperties(updatedPatient, patient, GeneralUtils.getInstance().getNullProperties(updatedPatient));
                      return patientRepository.save(updatedPatient);
                    })).map(ResponseEntity::ok)
            .switchIfEmpty(updatedPatientMono
                    .flatMap(patientRepository::save)
                    .map(updatedPatient -> ResponseEntity
                            .created(URI.create(""))
                            .lastModified(ZonedDateTime.now().toEpochSecond())
                            .body(updatedPatient)
                    ));
  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  public Mono<ResponseEntity<CPatient>> save(@RequestBody Mono<CPatient> patientMono) {
    return patientMono
            .doOnNext(patient -> patient.setId(IdType.newRandomUuid()))
            .flatMap(patientRepository::save)
            .map(ResponseEntity::ok);
  }

  @DeleteMapping("{id}")
  public Mono<ResponseEntity<Void>> delete(@PathVariable String id) {
    return patientRepository
            .deleteById(id)
            .map(i -> ResponseEntity.noContent().build());
  }

  @GetMapping("{id}")
  public Mono<ResponseEntity<CPatient>> getById(@PathVariable String id) {
    return patientRepository
            .findById(id)
            .map(ResponseEntity::ok)
            .defaultIfEmpty(ResponseEntity.notFound().build());
  }

  @GetMapping
  public Mono<ResponseEntity<List<CPatient>>> search(@RequestParam Map<String, String> params) {

    if (params.size() == 0) {
      // Return all entities if no search parameters specified
      return patientRepository
              .findAll()
              .collectList()
              .map(ResponseEntity::ok);
    }

    try {
      List<CriteriaDefinition> criteriaDefinitions = new ArrayList<>();
      for (Map.Entry<String, String> entry :
              params.entrySet()) {
        if (entry.getValue() == null) {
          throw new IllegalStateException("Search parameter value cannot be null");
        }
        criteriaDefinitions
                .add(getCriteriaDefinition(entry.getKey(), entry.getValue()));
      }

      return reactiveCassandraOperations
              .select(Query.query(criteriaDefinitions).withAllowFiltering(), CPatient.class)
              .collectList()
              .map(ResponseEntity::ok);
    } catch (IllegalStateException ex) {
      return Mono.just(ResponseEntity.badRequest().build());
    }
  }

  // FIXME: 19-Jul-18 Some of the properties do not work with search
  // Throwing an error status 500 with `No property <property-name> found on org.openmrs.model.CPatient!`
  private CriteriaDefinition getCriteriaDefinition(String key, String value) {
    Criteria criteria = Criteria.where(key);
    switch (key.toLowerCase()) {
      case "_id":
        criteria = Criteria.where("patientid");
        return criteria.is(value);
      case "address":
      case "communication":
      case "contact":
      case "contained":
      case "generalpractitioner":
      case "identifier":
      case "link":
      case "name":
      case "telecom":
      case "photo":
        return criteria.contains(value);
      case "id":
      case "animal":
      case "deceased":
      case "language":
      case "managingorganization":
      case "multiplebirth":
        // TODO: 12-Jul-18 Compare the supplied value with the target key instead of the whole JSON object
        return criteria.like(value);
      case "active":
      case "gender":
        return criteria.is(value);
      case "birthdate":
        return criteria.is(Date.from(Instant.parse(value)));
      default:
        throw new IllegalStateException("Unknown search parameter passed: " + key);
    }
  }
}
