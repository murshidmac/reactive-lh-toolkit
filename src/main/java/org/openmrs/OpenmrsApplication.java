package org.openmrs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenmrsApplication {

  public static void main(String[] args) {
    SpringApplication.run(OpenmrsApplication.class, args);
  }
}
