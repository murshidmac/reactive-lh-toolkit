package org.openmrs.converter;

import org.hl7.fhir.dstu3.model.Ratio;
import org.openmrs.annotation.CassandraConverter;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

@CassandraConverter
@ReadingConverter
public class RatioConverter implements Converter<String, Ratio> {

  @Override
  public Ratio convert(String source) {
    return ConvertUtils.getInstance().fromString(source, Ratio.class);
  }
}
