package org.openmrs.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.utilities.xhtml.XhtmlNode;
import org.openmrs.mixin.*;
import org.openmrs.model.CEncounter;
import org.openmrs.model.CObservation;
import org.openmrs.model.CPatient;
import org.openmrs.serializer.XhtmlNodeDeserializer;
import org.openmrs.serializer.XhtmlNodeSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JacksonConfig {
  public static final ObjectMapper mapper = configureObjectMapper();

  private static ObjectMapper configureObjectMapper() {
    return new ObjectMapper()
            .registerModule(
                    new SimpleModule()
                            .addSerializer(new XhtmlNodeSerializer())
                            .addDeserializer(XhtmlNode.class, new XhtmlNodeDeserializer())
            )
            .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .configure(DeserializationFeature.FAIL_ON_NULL_CREATOR_PROPERTIES, false)
            .setSerializationInclusion(JsonInclude.Include.NON_NULL)
            .addMixIn(Attachment.class, AttachmentMixin.class)
            .addMixIn(BackboneElement.class, BackboneElementMixin.class)
            .addMixIn(Base.class, BaseMixin.class)
            .addMixIn(BaseExtension.class, BaseExtensionMixin.class)
            .addMixIn(BaseNarrative.class, BaseNarrativeMixin.class)
            .addMixIn(BaseReference.class, BaseReferenceMixin.class)
            .addMixIn(BaseResource.class, BaseResourceMixin.class)
            .addMixIn(CEncounter.class, CEncounterMixin.class)
            .addMixIn(CodeableConcept.class, CodeableConceptMixin.class)
            .addMixIn(CObservation.class, CObservationMixin.class)
            .addMixIn(CPatient.class, CPatientMixin.class)
            .addMixIn(Element.class, ElementMixin.class)
            .addMixIn(Identifier.class, IdentifierMixin.class)
            .addMixIn(IdType.class, IdTypeMixin.class)
            .addMixIn(Meta.class, MetaMixin.class)
            .addMixIn(Narrative.class, NarrativeMixin.class)
            .addMixIn(Organization.class, OrganizationMixin.class)
            .addMixIn(Encounter.EncounterHospitalizationComponent.class, EncounterHospitalizationComponentMixin.class)
            .addMixIn(Encounter.EncounterLocationComponent.class, EncounterLocationComponentMixin.class)
            .addMixIn(Period.class, PeriodMixin.class)
            .addMixIn(Quantity.class, QuantityMixin.class)
            .addMixIn(Reference.class, ReferenceMixin.class)
            .addMixIn(Resource.class, ResourceMixin.class)
            .addMixIn(StringType.class, StringTypeMixin.class)
            .addMixIn(Type.class, TypeMixin.class)
            .addMixIn(XhtmlNode.class, XhtmlNodeMixin.class);
  }

  @Bean
  public ObjectMapper objectMapper() {
    return mapper;
  }
}
