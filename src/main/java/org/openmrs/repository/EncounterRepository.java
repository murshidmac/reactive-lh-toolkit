package org.openmrs.repository;

import org.openmrs.model.CEncounter;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface EncounterRepository extends ReactiveCrudRepository<CEncounter, String> {
}
