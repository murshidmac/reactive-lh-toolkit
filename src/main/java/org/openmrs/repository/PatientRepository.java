package org.openmrs.repository;

import org.openmrs.model.CPatient;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface PatientRepository extends ReactiveCrudRepository<CPatient, String> {

}
